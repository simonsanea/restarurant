package utils;

import model.Category;
import model.Menu;
import model.Product;

import java.sql.SQLOutput;

public class Utils {
    public static String getFloatPrice(long price) {
        String tempPrice = String.valueOf(price);
        String finalPrice = tempPrice.substring(0, tempPrice.length() - 2) + "." + tempPrice.substring(tempPrice.length() - 2);

        return finalPrice;
    }

    public static void displayMenu(Menu menu, int numberOfCharacters) {
        StringBuilder lines = new StringBuilder();
        for (int i = 0; i <= numberOfCharacters; i++) {
            lines.append("-");
        }
        for (Category category : menu.getCategoryList()) {
            System.out.println(category.getName());
            System.out.println(lines);
            for (Product product : category.getProducts()) {
                System.out.println(product.getName() + "     " + Utils.getFloatPrice(product.getPrice()));
            }
            System.out.println(lines);
        }
    }
}
