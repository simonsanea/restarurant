import model.Category;
import model.Menu;
import model.Product;
import utils.Utils;

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu("Meniu");
        Category category1 = new Category("Bauturi");
        Category category2 = new Category("Pizza");
        Product product1 = new Product("Cola", 1000);
        Product product2 = new Product( "Pizza capriciosa", 4030);
        category1.addItemsToMenu(product1);
        category2.addItemsToMenu(product2);
        menu.addCategory(category1);
        menu.addCategory(category2);

        Utils.displayMenu(menu, 50);
    }
}
