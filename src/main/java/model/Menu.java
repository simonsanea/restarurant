package model;

import utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private List<Category> categoryList = new ArrayList<>();
    private String title;

    public Menu(String title) {

        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void addCategory(Category category) {
        if (this.categoryList != null) {
            this.categoryList.add(category);
        }
    }
}
